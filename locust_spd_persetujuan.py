from locust import between, task, HttpUser
import json, os

user_name = os.environ.get('USER_NAME')
user_password = os.environ.get('USER_PASSWORD')

class WebUser(HttpUser):
    wait_time = between(0.5, 5)
    cookies = None

    requests_get = [
        ["web", "/home"],
        ["api", "/get-notification"],
        ["api", "/chart/pagu-skpd"],
        ["api", "/data/daftar-kegiatan-skpd/997"],
        ["web", "/otorisasi-spd"],
        ["api", "/data/skpd/all"],
        ["api", "/data/belum-otorisasi-spd/633"],
        ["api", "/data/sudah-otorisasi-spd/633"],
        # ["web", "/print/spd/552"],
    ]

    def get_home_public(self):
        path = '/'
        response = self.client.get(path)
        print("GET %s %s" % (path, response.status_code))

    def login(self):
        request_path = "/login"
        data = {"userName": user_name, "password": user_password, "tahunanggaran": "2021"}
        response = self.client.post(request_path, data=json.dumps(data),
                                    headers={"accept": "application/json", "content-type": "application/json"})
        if response:
            print("POST %s %s" % (request_path, response.status_code))
            self.cookies = response.cookies
        pass

    def on_start(self):
        self.get_home_public()
        self.login()

    @task
    def get_request(self):
        if self.cookies:
            length = len(self.requests_get)
            for i in range(length):
                request_type = self.requests_get[i][0]
                request_path = self.requests_get[i][1]

                if request_type == "web":
                    response = self.client.get(request_path)
                else:
                    response = self.client.get(request_path, headers={"accept": "application/json"})

                print("GET %s %s %s" % (request_type, request_path, response.status_code))
        pass
